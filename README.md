# Contour Energy

How energy of light and sound is perceived in the mind.

This is the Latex code for the corresponding document published at [Information contour](https://dedizier.de/customlabs/)

---

Long ago this Latex template was forked from the following.  Thanks and license prevails.

    LaTeX-Vorlage zur Projektdokumentation für Fachinformatiker Anwendungsentwicklung

[fiaevorlage]: http://fiae.link/LaTeXVorlageFIAE "Vorlage für die Projektdokumentation"

# Lizenz

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
LaTeX-Vorlage zur IHK-Projektdokumentation für Fachinformatiker Anwendungsentwicklung von [Stefan Macke](http://fiae.link/LaTeXVorlageFIAE) ist lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).
